#!/bin/sh
# Run this to generate all the initial makefiles, etc.

package="nautilus-media"
srcfile="audio-view/audio-view.c"

# include our .m4 macros
ACLOCAL_FLAGS="$ACLOCAL_FLAGS -I m4"
ACLOCAL_AMFLAGS="-I m4"

# source helper functions for autogen
. ./gst-autogen.sh

autogen_options $@

echo -n "+ check for build tools"
if test ! -z $NOCHECK; then echo ": skipped version checks"; else  echo; fi
version_check "autoconf" "$AUTOCONF autoconf autoconf-2.54 autoconf-2.53 autoconf-2.52" \
              "ftp://ftp.gnu.org/pub/gnu/autoconf/" 2 52 || DIE=1
version_check "automake" "$AUTOMAKE automake automake-1.7 automake-1.6 automake-1.5" \
              "ftp://ftp.gnu.org/pub/gnu/automake/" 1 5 || DIE=1
version_check "glib-gettextize" "" \
              "ftp://ftp.gnome.org/pub/gnome/sources/glib/" 2 0 0 || DIE=1
version_check "intltoolize" "" \
              "ftp://ftp.gnome.org/pub/gnome/sources/intltool/" 0 22 || DIE=1
version_check "libtool" "" \
              "ftp://ftp.gnu.org/pub/gnu/libtool/" 1 4 0 || DIE=1
version_check "pkg-config" "" \
              "http://www.freedesktop.org/software/pkgconfig" 0 8 0 || DIE=1

die_check $DIE

autoconf_2_52d_check || DIE=1
aclocal_check || DIE=1
autoheader_check || DIE=1

die_check $DIE

# if no arguments specified then this will be printed
if test -z "$*"; then
  echo "+ checking for autogen.sh options"
  echo "  This autogen script will automatically run ./configure as:"
  echo "  ./configure $CONFIGURE_DEF_OPT"
  echo "  To pass any additional options, please specify them on the $0"
  echo "  command line."
fi

toplevel_check $srcfile

if test -e acinclude.m4; then rm acinclude.m4; fi
tool_run "$aclocal" "$ACLOCAL_FLAGS"
tool_run "glib-gettextize" "-f"
tool_run "intltoolize" "--copy --force"
tool_run "libtoolize" "--copy --force"
tool_run "$autoheader"

tool_run "$autoconf"
tool_run "$automake" "-a -c"

test -n "$NOCONFIGURE" && {
  echo "skipping configure stage for package $package, as requested."
  echo "autogen.sh done."
  exit 0
}

echo "+ running configure ... "
test ! -z "$CONFIGURE_DEF_OPT" && echo "  ./configure default flags: $CONFIGURE_DEF_OPT"
test ! -z "$CONFIGURE_EXT_OPT" && echo "  ./configure external flags: $CONFIGURE_EXT_OPT"
echo

./configure $CONFIGURE_DEF_OPT $CONFIGURE_EXT_OPT || {
        echo "  configure failed"
        exit 1
}

echo
echo "Now type 'make' to compile $package."

