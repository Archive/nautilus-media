AC_INIT

PACKAGE=nautilus-media
VERSION=0.8.1
dnl RELEASE=0.`date +%Y%m%d_%H%M%S`
RELEASE=1

AC_DEFINE_UNQUOTED(RELEASE, "$RELEASE", [release of rpm package])
AC_SUBST(PACKAGE)
AC_SUBST(VERSION)
AC_SUBST(RELEASE)

AM_INIT_AUTOMAKE($PACKAGE, $VERSION)
AM_CONFIG_HEADER(config.h)

AC_PROG_CC
AM_PROG_LIBTOOL

dnl let make-triggered aclocal pick up our m4 dir
ACLOCAL_AMFLAGS="-I m4"
AC_SUBST(ACLOCAL_AMFLAGS)

dnl Check for pkgconfig first
AC_CHECK_PROG(HAVE_PKGCONFIG, pkg-config, yes, no)

dnl Give error and exit if we don't have pkgconfig
if test "x$HAVE_PKGCONFIG" = "xno"; then
  AC_MSG_ERROR(you need to have pkgconfig installed !)
fi

dnl Now we're ready to ask for gstreamer libs and cflags
dnl And we can also ask for the right version of gstreamer
HAVE_GST=no

GST_REQ=0.8.0
dnl start with 0.8
GST_MAJORMINOR=0.8
PKG_CHECK_MODULES(GST, \
  gstreamer-$GST_MAJORMINOR >= $GST_REQ \
  gstreamer-gconf-$GST_MAJORMINOR >= $GST_REQ \
  gstreamer-libs-$GST_MAJORMINOR >= $GST_REQ,
  HAVE_GST=yes,HAVE_GST=no)

dnl fall back to 0.7
dnl if test "x$HAVE_GST" = "xno"; then
dnl   GST_MAJORMINOR=0.7
dnl   PKG_CHECK_MODULES(GST, \
dnl     gstreamer-$GST_MAJORMINOR >= $GST_REQ \
dnl     gstreamer-gconf-$GST_MAJORMINOR >= $GST_REQ \
dnl     gstreamer-libs-$GST_MAJORMINOR >= $GST_REQ,
dnl   HAVE_GST=yes,HAVE_GST=no)
dnl fi

dnl Give error and exit if we don't have gstreamer
if test "x$HAVE_GST" = "xno"; then
  AC_MSG_ERROR(you need gstreamer development packages installed !)
fi

dnl make GST_CFLAGS and GST_LIBS available
AC_SUBST(GST_CFLAGS)
AC_SUBST(GST_LIBS)

dnl for test apps we want gnome stuff
PKG_CHECK_MODULES(GNOME, libgnome-2.0 libgnomeui-2.0 eel-2.0,
                  HAVE_GNOME=yes, HAVE_GNOME=no)

dnl Give error and exit if we don't have gnome ui
if test "x$HAVE_GNOME" = "xno"; then
  AC_MSG_ERROR(you need libgnome pkgs installed !)
fi

dnl make GNOME_CFLAGS and GNOME_LIBS available
AC_SUBST(GNOME_CFLAGS)
AC_SUBST(GNOME_LIBS)

dnl And we want Nautilus as well
PKG_CHECK_MODULES(NAUTILUS, 
                  libnautilus,
                  HAVE_NAUTILUS=yes, HAVE_NAUTILUS=no)

dnl Give error and exit if we don't have nautilus
if test "x$HAVE_NAUTILUS" = "xno"; then
  AC_MSG_ERROR(you need nautilus development packages installed !)
fi

dnl make NAUTILUS_CFLAGS and NAUTILUS_LIBS available
AC_SUBST(NAUTILUS_CFLAGS)
AC_SUBST(NAUTILUS_LIBS)

dnl intltool stuff
AC_PROG_INTLTOOL(0.18)
dnl AM_WITH_NLS
GETTEXT_PACKAGE=nautilus-media
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [gettext package])
ALL_LINGUAS="ar am ar az be be@latin bg bn bs ca cs cy da de dz el en_CA en_GB es et eu fa fi fr ga gl gu he hi hr hu id is it ja ko li lt lv mi mk ml mn ms nb ne nl nn no or pa pl pt pt_BR ro ru rw sk sl sq sr sr@Latn sv ta th tk tr uk vi zh_CN zh_TW"
AM_GLIB_GNU_GETTEXT

dnl get a good @LIBEXECDIR@ translation
AS_AC_EXPAND(LIBEXECDIR, $libexecdir)
AC_SUBST(LIBEXECDIR)

dnl get a good @BONOBODIR@ translation
AS_AC_EXPAND(BONOBODIR, $libdir/bonobo)
AC_SUBST(BONOBODIR)

dnl uninstalled DATADIR
AC_DEFINE_UNQUOTED(DATADIR_UNINST,"`pwd`", [path to uninstalled data dir])
AC_SUBST(DATADIR_UNINST)

dnl PIXMAPSDIR
AS_AC_EXPAND(PIXMAPSDIR, $datadir/pixmaps/$PACKAGE)
AC_DEFINE_UNQUOTED(PIXMAPSDIR, "$PIXMAPSDIR", 
                   [directory where pixmaps are stored])
						 
dnl FIXME: at some point we'll require autoconf 2.5x and then we can
dnl use AC_HELP_STRING here for aesthetic pleasure
AC_ARG_ENABLE(test-view,
              [  --enable-test-view      build the nautilus test view],
              enable_test_view=yes, enable_test_view=no)
AM_CONDITIONAL(BUILD_TEST_VIEW, test x$enable_test_view = xyes)
AC_SUBST(BUILD_TEST_VIEW)
#if test x$enable_test_view = xyes; then
#    AC_DEFINE(BUILD_TEST_VIEW,1,[defined if test view])
#fi

dnl where do we install our gconf thumbnailing schemas ?
AC_PATH_PROG(GCONFTOOL, gconftool-2, no)
if test x$GCONFTOOL = xno; then
  AC_MSG_ERROR([You need gconftool-2 to install the GConf schemas])
fi
AM_GCONF_SOURCE_2()

dnl do we build with deprecation on ?
dnl FIXME: put this into a flag depending on the nano version
#DEPRECATE="-DG_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED -DGNOME_DISABLE_DEPRECATED"
DEPRECATE=""
AC_SUBST(DEPRECATE)

dnl check for da man
AC_MSG_CHECKING([for da man])
AC_MSG_RESULT([campd])

AC_OUTPUT([
  Makefile 
  m4/Makefile
  media-info/Makefile
  audio-properties-view/Makefile
  audio-properties-view/Nautilus_View_audio_properties.server.in
  audio-view/Makefile audio-view/Nautilus_View_audio.server.in
  test-view/Makefile test-view/Nautilus_View_test.server.in
  thumbnail/Makefile
  pixmaps/Makefile
  po/Makefile.in 
  nautilus-media.spec
])

