# Brazilian Portuguese translation for nautilus-media
# Copyright (C) 2003 Gustavo Noronha Silva
# This file is distributed under the same license as the nautilus-media package.
# Gustavo Noronha Silva <kov@debian.org>, 2003
#
msgid ""
msgstr ""
"Project-Id-Version: nautilus-media\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-09-13 08:37+0200\n"
"PO-Revision-Date: 2004-03-10 15:16-0500\n"
"Last-Translator: Raphael Higino <raphaelh@uai.com.br>\n"
"Language-Team: GNOME-BR <gnome-l10n-br@listas.cipsga.org.br>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: audio-properties-view/Nautilus_View_audio_properties.server.in.in.h:1
#: audio-view/Nautilus_View_audio.server.in.in.h:1
msgid "Audio"
msgstr "Áudio"

#: audio-properties-view/Nautilus_View_audio_properties.server.in.in.h:2
msgid "Audio Properties content view component"
msgstr "Componente de visualização de conteúdo Propriedades de Áudio"

#: audio-properties-view/Nautilus_View_audio_properties.server.in.in.h:3
msgid "Nautilus Audio Properties view"
msgstr "Visualização de Propriedades de Áudio Nautilus"

#: audio-properties-view/audio-properties-view.glade.h:1
msgid "*"
msgstr "*"

#: audio-properties-view/audio-properties-view.glade.h:2
msgid "<span weight=\"bold\">Album:</span>"
msgstr "<span weight=\"bold\">Álbum:</span>"

#: audio-properties-view/audio-properties-view.glade.h:3
msgid "<span weight=\"bold\">Artist:</span>"
msgstr "<span weight=\"bold\">Artista:</span>"

#: audio-properties-view/audio-properties-view.glade.h:4
msgid "<span weight=\"bold\">Bit rate:</span>"
msgstr "<span weight=\"bold\">Bit rate:</span>"

#: audio-properties-view/audio-properties-view.glade.h:5
msgid "<span weight=\"bold\">Comments:</span>"
msgstr "<span weight=\"bold\">Comentários:</span>"

#: audio-properties-view/audio-properties-view.glade.h:6
msgid "<span weight=\"bold\">Format:</span>"
msgstr "<span weight=\"bold\">Formato:</span>"

#: audio-properties-view/audio-properties-view.glade.h:7
msgid "<span weight=\"bold\">Length:</span>"
msgstr "<span weight=\"bold\">Tamanho:</span>"

#: audio-properties-view/audio-properties-view.glade.h:8
msgid "<span weight=\"bold\">Title:</span>"
msgstr "<span weight=\"bold\">Título:</span>"

#: audio-properties-view/audio-properties-view.glade.h:9
msgid "<span weight=\"bold\">Track:</span>"
msgstr "<span weight=\"bold\">Faixa:</span>"

#: audio-properties-view/audio-properties-view.glade.h:10
msgid "<span weight=\"bold\">_Artist:</span>"
msgstr "<span weight=\"bold\">_Artista:</span>"

#: audio-properties-view/audio-properties-view.glade.h:11
msgid "Audio Properties"
msgstr "Propriedades de Áudio"

#: audio-properties-view/nautilus-audio-properties-view.c:284
msgid "loading..."
msgstr "carregando..."

#: audio-properties-view/nautilus-audio-properties-view.c:297
msgid "URI currently displayed"
msgstr "URI atualmente exibida"

#: audio-properties-view/audio-properties-view.c:152
#: audio-properties-view/audio-properties-view.c:156
#: audio-properties-view/audio-properties-view.c:160
#: audio-properties-view/audio-properties-view.c:161
msgid "None"
msgstr "Nenhum"

#: audio-properties-view/audio-properties-view.c:170
msgid "mono"
msgstr "mono"

#: audio-properties-view/audio-properties-view.c:171
msgid "stereo"
msgstr "stereo"

#: audio-properties-view/audio-properties-view.c:172
msgid "unknown"
msgstr "desconhecido"

#: audio-properties-view/audio-properties-view.c:173
#, c-format
msgid "%d channel"
msgid_plural "%d channels"
msgstr[0] "%d canal"
msgstr[1] "%d canais"

#: audio-properties-view/audio-properties-view.c:185
#, c-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] "%d minuto"
msgstr[1] "%d minutos"

#. Translators: plural form depends on the decimal part
#: audio-properties-view/audio-properties-view.c:187
#, c-format
msgid "%02d.%03d seconds"
msgid_plural "%02d.%03d seconds"
msgstr[0] "%02d.%03d segundos"
msgstr[1] "%02d.%03d segundos"

#. Translators: this is composed of '%d minute' and '%02d.%03d seconds'; change order if needed
#: audio-properties-view/audio-properties-view.c:189
#, c-format
msgid "%1$s %2$s"
msgstr "%1$s %2$s"

#: audio-properties-view/audio-properties-view.c:259
#: audio-properties-view/audio-properties-view.c:260
#: audio-properties-view/audio-properties-view.c:261
#: audio-properties-view/audio-properties-view.c:263
#: audio-properties-view/audio-properties-view.c:264
#: audio-properties-view/audio-properties-view.c:265
#: audio-view/audio-view.c:314 audio-view/audio-view.c:384
#: audio-view/audio-view.c:928 audio-view/audio-view.c:932
#: audio-view/audio-view.c:933
msgid "Unknown"
msgstr "Desconhecido"

#: audio-view/Nautilus_View_audio.server.in.in.h:2
msgid "Audio Viewer"
msgstr "Visualizador de Áudio"

#: audio-view/Nautilus_View_audio.server.in.in.h:3
msgid "Audio view component"
msgstr "Componente de visualização de áudio"

#: audio-view/Nautilus_View_audio.server.in.in.h:4
msgid "Audio view component's factory"
msgstr "Fábrica de componente de visualização de áudio"

#: audio-view/Nautilus_View_audio.server.in.in.h:5
msgid "Audio view factory"
msgstr "Fábrica de visualização de áudio"

#: audio-view/Nautilus_View_audio.server.in.in.h:6
msgid "Nautilus Audio view"
msgstr "Visualização de Áudio Nautilus"

#: audio-view/Nautilus_View_audio.server.in.in.h:7
msgid "View as Audio"
msgstr "Ver como Áudio"

#: audio-view/audio-view.c:286
msgid "Ogg/Vorbis"
msgstr "Ogg/Vorbis"

#: audio-view/audio-view.c:289
msgid "FLAC"
msgstr "FLAC"

#: audio-view/audio-view.c:295
msgid "MPEG"
msgstr "MPEG"

#: audio-view/audio-view.c:297
msgid "WAVE"
msgstr "WAVE"

#: audio-view/audio-view.c:301 audio-view/audio-view.c:303
#: audio-view/audio-view.c:305 audio-view/audio-view.c:307
msgid "Amiga mod"
msgstr "Amiga mod"

#: audio-view/audio-view.c:309
msgid "Apple AIFF"
msgstr "Apple AIFF"

#: audio-view/audio-view.c:311
msgid "MIDI"
msgstr "MIDI"

#: audio-view/audio-view.c:313
msgid "ulaw audio"
msgstr "ulaw audio"

#: audio-view/audio-view.c:387 audio-view/audio-view.c:390
msgid "Artist: "
msgstr "Artista: "

#: audio-view/audio-view.c:388 audio-view/audio-view.c:391
msgid "Title: "
msgstr "Título: "

#: audio-view/audio-view.c:444 audio-view/audio-view.c:660
#: audio-view/audio-view.c:693
msgid "Make a selection first !"
msgstr "Escolha alguma coisa primeiro !"

#: audio-view/audio-view.c:465
msgid "<span size=\"larger\">Unknown</span>"
msgstr "<span size=\"larger\">Desconhecido</span>"

#: audio-view/audio-view.c:508
msgid "ERROR: no information yet"
msgstr "ERRO: não há informação ainda"

#. FIXME: handleme
#: audio-view/audio-view.c:550
#, c-format
msgid "ERROR: %s"
msgstr "ERRO: %s"

#. status update
#: audio-view/audio-view.c:558 audio-view/audio-view.c:580
#, c-format
msgid "Playing %s"
msgstr "Tocando %s"

#: audio-view/audio-view.c:721 audio-view/audio-view.c:789
msgid "Stopped."
msgstr "Parado."

#: audio-view/audio-view.c:997
#, c-format
msgid "Sorry, but there was an error reading %s."
msgstr "Desculpe, mas houve um erro lendo %s."

#: audio-view/audio-view.c:1082
msgid "File"
msgstr "Arquivo"

#: audio-view/audio-view.c:1094
msgid "Type"
msgstr "Tipo"

#: audio-view/audio-view.c:1106
msgid "Length"
msgstr "Tamanho"

#: audio-view/audio-view.c:1118
msgid "Bitrate"
msgstr "Bitrate"

#. The metadata column
#: audio-view/audio-view.c:1128
msgid "Metadata"
msgstr "Metadados"

#. get a status area
#: audio-view/audio-view.c:1263
msgid "Not playing"
msgstr "Não tocando"

#: audio-view/nautilus-audio-view-ui.xml.h:1
msgid "Sample"
msgstr "Exemplo"

#: audio-view/nautilus-audio-view-ui.xml.h:2
msgid "This is a sample merged menu item"
msgstr "Essa é um item de menu de exemplo"

#: audio-view/nautilus-audio-view-ui.xml.h:3
msgid "This is a sample merged toolbar button"
msgstr "Esse é um botão de barra de ferramentas embutido de exemplo"

#: audio-view/nautilus-audio-view-ui.xml.h:4
#: test-view/nautilus-test-view-ui.xml.h:4
msgid "_File"
msgstr "_Arquivo"

#: audio-view/nautilus-audio-view-ui.xml.h:5
msgid "_Sample"
msgstr "Exe_mplo"

#: test-view/Nautilus_View_test.server.in.in.h:1
msgid "Nautilus Test view"
msgstr "Visualização de Teste do Nautilus"

#: test-view/Nautilus_View_test.server.in.in.h:2
msgid "Nautilus Test view factory"
msgstr "Fábrica da visualização de teste do Nautilus"

#: test-view/Nautilus_View_test.server.in.in.h:3
#: test-view/nautilus-test-view-ui.xml.h:1
msgid "Test"
msgstr "Teste"

#: test-view/Nautilus_View_test.server.in.in.h:4
msgid "Test Viewer"
msgstr "Visualização Teste"

#: test-view/Nautilus_View_test.server.in.in.h:5
msgid "Test view component"
msgstr "Componente de visualização de test"

#: test-view/Nautilus_View_test.server.in.in.h:6
msgid "Test view component's factory"
msgstr "Fábrica do componente de visualização de teste"

#: test-view/Nautilus_View_test.server.in.in.h:7
msgid "View as Test"
msgstr "Ver como Teste"

#: test-view/nautilus-test-view-ui.xml.h:2
msgid "This is a test merged menu item"
msgstr "Esse é um item de menu embutido de teste"

#: test-view/nautilus-test-view-ui.xml.h:3
msgid "This is a test merged toolbar button"
msgstr "Esse é um botão de barra de ferramentas embutido"

#: test-view/nautilus-test-view-ui.xml.h:5
msgid "_Test"
msgstr "_Teste"

#: test-view/nautilus-test-view.c:80
#, c-format
msgid ""
"%s\n"
"\n"
"This is a test Nautilus content view component."
msgstr ""
"%s\n"
"\n"
"Esse é um componente de visualização de conteúdo do Nautilus."

#: test-view/nautilus-test-view.c:146
#, c-format
msgid ""
"%s\n"
"\n"
"You selected the Test menu item."
msgstr ""
"%s\n"
"\n"
"Você selecionou o item de menu de Teste"

#: test-view/nautilus-test-view.c:150
#, c-format
msgid ""
"%s\n"
"\n"
"You clicked the Test toolbar button."
msgstr ""
"%s\n"
"\n"
"Você clicou no botão de Teste da barra de ferramentas"

#: test-view/nautilus-test-view.c:206
msgid "(none)"
msgstr "(nenhum)"
